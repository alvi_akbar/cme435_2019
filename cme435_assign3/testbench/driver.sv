`include "testbench/transaction.sv"

class driver;
  //creating virtual interface handle
  virtual intf vif;

  //used to count the number of transactions
  int no_transactions;
  transaction trans;

  //creating mailbox handle
  mailbox gen2driv;


  //constructor
  function new(virtual intf vif, mailbox gen2driv);
    //getting the interface
    this.vif = vif;

    //getting the mailbox handles from environment
    this.gen2driv = gen2driv;

  endfunction

  //drive the transaction items to interface signals
  task main;

    forever begin
      gen2driv.get(trans);

      @(posedge vif.clk);
      vif.valid <= 1;
      vif.a     <= trans.a;
      vif.b     <= trans.b;

      @(posedge vif.clk); vif.valid <= 0;
      trans.c = vif.c;

      @(posedge vif.clk);
      trans.display("[ Driver ]");
      no_transactions++;
    end

  endtask

endclass
