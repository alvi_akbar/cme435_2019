`include "testbench/interface.sv"
`include "testbench/testbench.sv" 

module tbench_top;
    
    //clock and reset signal declaration
  bit clk;
  bit reset;
  
  event error;

  //clock generation
  always #5 clk = ~clk;
  
  //reset Generation
  initial begin
    reset    = 1;
    #5 reset = 0;
  end

  // Increment the error counter when error occurs.
  always @(error)
  begin
    #0 i_intf.error_count = i_intf.error_count + 1;
    $display("Time:%0d ns ERROR RECEIVED", $time);
  end

  intf i_intf (clk, reset); // Creating instance of interface, inorder to connect DUT and testcase
  
  testbench t1 (i_intf);  // Testcase instance, interface handle is passed to test as an argument
  
  dut_top dut(i_intf); // DUT instance, interface signals are connected to the DUT ports

  initial begin
    $dumpfile("dump.vcd"); $dumpvars; //enabling the wave dump
  end
    
endmodule
