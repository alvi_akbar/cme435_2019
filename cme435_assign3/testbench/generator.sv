`include "testbench/transaction.sv"
class generator;
  //declaring transaction class
  // rand transaction trans;
  transaction trans;

  mailbox gen2driv;

  //repeat count, to specify number of items to generate
  int repeat_count;

  //event, to indicate the end of transaction generation
  event ended; 

  //constructor
  function new(mailbox gen2driv);
    //getting the mailbox handle from env.
    this.gen2driv = gen2driv;
  endfunction

  //main task, generates (creates and randomizes) transaction packets
  task main();
    repeat(repeat_count) begin
      trans = new();

      // if( !trans.randomize() )
        // $fatal("Gen:: trans randomization failed");

      trans.display("[ Generator ]");

      gen2driv.put(trans);

    end
    -> ended; //trigger end of generation
  endtask

endclass
