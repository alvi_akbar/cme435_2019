`include "testbench/transaction.sv"
`include "testbench/generator.sv"
`include "testbench/driver.sv"
`include "testbench/monitor.sv"
`include "scoreboard.sv" 

class environment;

  //generator instance
  generator gen;
  driver driv;
  monitor mon;
  scoreboard scb; 

  //mailbox handle's
  mailbox gen2driv;
  mailbox mon2scb;

  //virtual interface
  virtual intf vif;

  //constructor
  function new(virtual intf vif);
    //get the interface from test
    this.vif = vif;

    //creating the mailbox (Same handle will be shared across generator and driver)
    gen2driv = new();
    mon2scb = new();

    // creating generator
    gen = new(gen2driv);
    driv = new(vif, gen2driv);
    mon = new(vif,mon2scb);
    scb = new(mon2scb);

  endfunction

  task pre_test();
    $display("%0d : Environment : start of pre_test()", $time);
    reset();
    $display("%0d : Environment : end of pre_test()", $time);
  endtask

  task test();
    $display("%0d : Environment : start of test()", $time);

    fork
      gen.main();
      driv.main();
      mon.main();
      scb.main();
    join_any
    
    wait(gen.ended.triggered); // optional
    wait(gen.repeat_count == driv.no_transactions); 
    wait(gen.repeat_count == scb.no_transactions);
    $display("%0d : Environment : end of test()", $time);
  endtask

  task post_test();
    $display("%0d : Environment : start of post_test()", $time);
    
    if(vif.error_count != 0)
      $display("############# TEST FAILED #############");
    else
      $display("############# TEST PASSED #############");

    $display("%0d : Environment : end of post_test()", $time);
  endtask

  task reset();
    wait(vif.reset);
    $display("[ ENVIRONMENT ] ----- Reset Started -----");

    vif.a <= 0;
    vif.b <= 0;
    vif.valid <= 0;
    vif.error_count <= 0;

    wait(!vif.reset);

    $display("[ ENVIRONMENT ] ----- Reset Ended -------");
    endtask

  //run task
  task run;
    $display("%0d : Environment : start of run()", $time);

    pre_test();
    test();
    post_test();

    $display("%0d : Environment : end of run()", $time);

    // $finish;
  endtask 


endclass
