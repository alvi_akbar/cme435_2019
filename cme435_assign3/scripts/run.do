onbreak {resume}
# create library
if [file exists work] {
 vdel -all
}

vlib work
vmap work work

# compiling all dut source files
vlog dut/*.sv

# compiling all testbench source files
vlog testbench/*.sv 

# optimize design
vopt +acc tbench_top -o tbench_top_opt
vsim tbench_top_opt

# wave signals
add wave /tbench_top/*

add log -r /*

# run simulation
# run –all

run 3000

# quit –f