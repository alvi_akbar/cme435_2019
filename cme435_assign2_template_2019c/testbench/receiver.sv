module receiver(clk, data, ready, read, port); 

input clk;
input [7:0]data;
input ready; 
output read; 
input port;
reg read; 
wire [7:0] port;

reg [0:7] mem [0:65];
integer j, delay;

// Start collecting packet
always @(posedge ready)
begin
 delay = {$random()}%5+1;
 repeat(delay)@(negedge clk);

 j=0;
 @(negedge clk);
 read=1'b1;
 while(ready==1'b1)
 begin
 @(negedge clk);
 mem[j]=data;
 j=j+1;
 $display(" RECV BYTE at PORT[%d] %b",port,data);
 end //while
 read=1'b0;
end


endmodule: receiver