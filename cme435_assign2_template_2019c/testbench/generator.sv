`define SMALL 0
`define MEDIUM 0
`define LARGE 0

module generator();

integer payload_size;   // Control field for the payload size 
integer parity_type;    // Control field for the parity type

reg [0:7] uid;          // Unique id field to identify the packet
reg [7:0] len;
reg [7:0] Da;
reg [7:0] Sa;
reg [0:7] pkt [0:64];   // Maximum packet size
reg [0:7] parity;

initial
  uid = 0;

task randomize();
begin
  uid = uid +1;
  parity_type = {$random}%2; // 0 and 1 are selected randomly
  payload_size ={$random}%3; // 0,1,2 are selected randomly
  Da = $root.tbench_top.mem[({$random}%3)]; //{$random}%3;// 0,1,2,3 are selected randomly
  Sa = $random;

  if(payload_size == `SMALL)
      len = {$random}%10; 
  else if(payload_size == `MEDIUM)
      len = 10+{$random}%10;
  else if(payload_size == `LARGE)
      len = 20+{$random()}%10;
  else 
      len = 30+{$random()}%10;

  if(parity_type == 0)
      parity = 8'b0;
  else
      parity = 8'b1;
end
endtask

task packing();
integer i;
begin
  pkt[0]=Da;
  pkt[1]=Sa;
  pkt[2]=len;

  $display("[PACKING] pkt[0] is Da %b %d Sa %b %d len %b %d ",pkt[0], Da, Sa, Sa, len, len);
    
  for (i = 0;i<len+3;i=i+1)
      pkt[i+3]=$random();
  pkt[3] = uid;
  pkt[i+3] = parity ^ parity_cal(0);
end
endtask

function [0:7] parity_cal(input dummy);
  integer i;
  reg [0:7] result ;
  begin
  result = 8'hff;

  for (i = 0;i<len+4;i=i+1)
  begin
      result = result ^ pkt[i];
  end
      parity_cal=result;
  end
endfunction

endmodule