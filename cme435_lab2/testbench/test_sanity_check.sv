module testbench(
  clock, 
  packet_valid, 
  data, 
  data_0, 
  data_1, 
  data_2, 
  data_3, 
  ready_0, 
  ready_1, 
  ready_2, 
  ready_3, 
  read_0, 
  read_1, 
  read_2, 
  read_3,
  mem_en, 
  mem_rd_wr, 
  mem_add, 
  mem_data);
 
input clock;
output packet_valid;
output [7:0] data;
input [7:0] data_0;
input [7:0] data_1;
input [7:0] data_2;
input [7:0] data_3;
input ready_0;
input ready_1;
input ready_2;
input ready_3;
output read_0;
output read_1;
output read_2;
output read_3;
output mem_en;
output mem_rd_wr;
output [1:0] mem_add;
output [7:0] mem_data;

wire mem_en;
reg mem_rd_wr;
wire [1:0] mem_add;
wire [7:0] mem_data;

integer pkt_size;

environment env (
 clock,
 packet_valid,
 data,
 data_0,
 data_1,
 data_2,
 data_3,
 ready_0,
 ready_1,
 ready_2,
 ready_3,
 read_0,
 read_1,
 read_2,
 read_3,
 mem_en,
 mem_rd_wr,
 mem_add,
 mem_data);

// TEST 8.1.1
// Testing Ready Bit Assestion Success
always@(negedge clock)
if(packet_valid == 1'b1 && mem_data == env.gen.Da)
  begin
    $display("\n\nTEST 8.1.1: Ready Assertion\n\n");
    $display("Test if ready_0 is asserted after mem_data equals dest address\n");

    assert(ready_0 == 1'b1) $display("Success: ready_0 is asserted after mem_data equals dest addr\n");
    else begin
      $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
      -> env.error;
    end
  end

always@(negedge clock)
if(packet_valid == 1'b1 && mem_data == env.gen.Da)
  begin
    $display("\n\nTEST 8.1.1: Ready Assertion\n\n");
    $display("Test if ready_1 is asserted after mem_data equals dest address\n");

    assert(ready_1 == 1'b1) $display("Success: ready_1 is asserted after mem_data equals dest addr\n");
    else begin
      $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
      -> env.error;
    end
  end


always@(negedge clock)
if(packet_valid == 1'b1 && mem_data == env.gen.Da)
  begin
    $display("\n\nTEST 8.1.1: Ready Assertion\n\n");
    $display("Test if ready_2 is asserted after mem_data equals dest address\n");

    assert(ready_2 == 1'b1) $display("Success: ready_2 is asserted after mem_data equals dest addr\n");
    else begin
      $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
      -> env.error;
    end
  end

always@(negedge clock)
if(packet_valid == 1'b1 && mem_data == env.gen.Da)
  begin
    $display("\n\nTEST 8.1.1: Ready Assertion\n\n");
    $display("Test if ready_3 is asserted after mem_data equals dest address\n");

    assert(ready_3 == 1'b1) $display("Success: ready_3 is asserted after mem_data equals dest addr\n");
    else begin
      $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
      -> env.error;
    end
  end

// TEST 8.1.2
// Testing Correct Port Mapping and not corrupted
always@(negedge clock)
if(ready_0 == 1'b1 && read_0 == 1'b1)
  begin
    repeat(4) @(posedge clock);
    $display("\n\nTEST 8.1.2: Port Mapping and Valid Packet\n\n");
    $display("Test if incoming packet is routed to correct port without being corrupted\n");

    assert(data_0 == env.mon.data_0) $display("Success: packet forwarded to port 0 and valid\n");
    else begin
      $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
      -> env.error;
    end
  end

  // TEST 8.1.2
  // Testing Correct Port Mapping and not corrupted
  always@(negedge clock)
  if(ready_1 == 1'b1 && read_1 == 1'b1)
    begin
      repeat(4) @(posedge clock);
      $display("\n\nTEST 8.1.2: Port Mapping and Valid Packet\n\n");
      $display("Test if incoming packet is routed to correct port without being corrupted\n");
  
      assert(data_1 == env.mon.data_1) $display("Success: packet forwarded to port 1 and valid\n");
      else begin
        $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
        -> env.error;
      end
    end

    // TEST 8.1.2
  // Testing Correct Port Mapping and not corrupted
  always@(negedge clock)
  if(ready_2 == 1'b1 && read_2 == 1'b1)
    begin
      repeat(4) @(posedge clock);
      $display("\n\nTEST 8.1.2: Port Mapping and Valid Packet\n\n");
      $display("Test if incoming packet is routed to correct port without being corrupted\n");
  
      assert(data_2 == env.mon.data_2) $display("Success: packet forwarded to port 2 and valid\n");
      else begin
        $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
        -> env.error;
      end
    end

    // TEST 8.1.2
  // Testing Correct Port Mapping and not corrupted
  always@(negedge clock)
  if(ready_3 == 1'b1 && read_3 == 1'b1)
    begin
      repeat(4) @(posedge clock);
      $display("\n\nTEST 8.1.2: Port Mapping and Valid Packet\n\n");
      $display("Test if incoming packet is routed to correct port without being corrupted\n");
  
      assert(data_3 == env.mon.data_3) $display("Success: packet forwarded to port 3 and valid\n");
      else begin
        $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
        -> env.error;
      end
    end

  // TEST 10
  // Testing Min Payload Size
  initial begin
  #1000 pkt_size = 1;

  @(negedge clock);
  if(ready_2 == 1'b1 && read_2 == 1'b1)
    begin
      repeat(4) @(posedge clock);
      $display("\n\nTEST 10: Min Payload Size\n\n");
      $display("Test Min Packet Size\n");
  
      assert(data_2 == env.mon.data_2) $display("Success: Min Payload Size Successful\n");
      else begin
        $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
        -> env.error;
      end
    end
  end

  // TEST 10
  // Testing Max Payload Size
  initial begin
    #880 pkt_size = 255;
    @(negedge clock);
    if(ready_1 == 1'b1 && read_1 == 1'b1)
      begin
        repeat(4) @(posedge clock);
        $display("\n\nTEST 10: Max Packet Size\n\n");
        $display("Test Max Packet Size\n");
    
        assert(data_1 == env.mon.data_1) $display("Success: Max Packet Size Successful\n");
        else begin
          $error("Failed for packet with uid: %0d\n", env.gen.pkt[3]);
          -> env.error;
        end
      end
    end
    
initial
begin
 $display("******************* Start of testcases ****************");

 pkt_size = 0;

 env.run();
end

initial
begin
  #2000
  env.finish();
end

final
 $display("******************** End of testcases *****************");

endmodule : testbench