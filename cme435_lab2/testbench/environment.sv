module environment(
  clock,
  packet_valid,
  data,
  data_0,
  data_1,
  data_2,
  data_3,
  ready_0,
  ready_1,
  ready_2,
  ready_3,
  read_0,
  read_1,
  read_2,
  read_3,
  mem_en,
  mem_rd_wr,
  mem_add,
  mem_data
);
 
input clock;
output packet_valid;
output [7:0] data;
input [7:0] data_0;
input [7:0] data_1;
input [7:0] data_2;
input [7:0] data_3;
input ready_0;
input ready_1;
input ready_2;
input ready_3;
output read_0;
output read_1;
output read_2;
output read_3;
output mem_rd_wr;
output mem_en;
output [1:0] mem_add;
output [7:0] mem_data;
 
reg pkt_status;
reg mem_en; 
reg mem_rd_wr; 
reg [1:0] mem_add; 
reg [7:0] mem_data;
reg [7:0] mem[3:0];

// Driver instance 
driver dr(clock, packet_valid, data, busy);

integer no_of_pkt = 4; // Assigning a default value less than 10

// Generator Instance
generator gen();

// Monitor instance
monitor mon(clock,
 data_0,
 data_1,
 data_2,
 data_3,
 ready_0,
 ready_1,
 ready_2, 
 ready_3,
 read_0,
 read_1,
 read_2,
 read_3);
 
 // Scoreboard Instance 
 scoreboard sb();

task reset_dut();
  $display("%0d : Environment : start of reset() method", $time);
  mem_en = 0;
  #0 -> $root.tbench_top.reset_trigger;
  @ ($root.tbench_top.reset_done);

  $display("%0d : Environment : end of reset() method", $time);
endtask : reset_dut


task cfg_dut();
  $display("%0d : Environment : start of cfg_dut() method", $time);
  
  mem[0] = $random;
  mem[1] = $random;
  mem[2] = $random;
  mem[3] = $random;
  mem_en = 0;

  reset_dut();
  repeat(9) @(negedge clock);

  mem_en = 1;

  @(negedge clock);
  mem_rd_wr = 1;
  mem_add = 0;
  mem_data = mem[0];
  mem_rd_wr = 1;
  mem_add = 0;
  mem_data = mem[0];

  @(negedge clock);
  mem_rd_wr = 1;
  mem_add = 1;
  mem_data = mem[1];

  @(negedge clock);
  mem_rd_wr = 1;
  mem_add = 2;
  mem_data = mem[2];

  @(negedge clock);
  mem_rd_wr = 1;
  mem_add = 3;
  mem_data = mem[3];

  @(negedge clock);
  mem_en=0;
  mem_rd_wr = 0;
  mem_add = 0;
  mem_data = 0; 
  
  $display("%0d : Environment : end of cfg_dut() method", $time);
endtask : cfg_dut

integer delay;

task run();
  $display("%0d : Environment : start of run() method", $time);
  reset_dut();
  cfg_dut();
  delay={$random()}%4;
  dr.gen_and_drive(no_of_pkt);
  #2000 $finish;
  
  $display("%0d : Environment : end of run() method", $time);
endtask : run

integer error_count=0;
event error;

// Increment the error counter when error occurs.
always@(error)
begin
  #0 error_count = error_count+1;
  $display("Time:%0d ns ERROR RECEIVED", $time);
end

// Display the test results.
task finish();
begin
  if(error_count != 0)
    $display("############# TEST FAILED #############");
  else
    $display("############# TEST PASSED #############");   
  end
endtask


endmodule : environment