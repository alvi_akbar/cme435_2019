module driver(clock, packet_valid, data, busy); 

output packet_valid; 
output [7:0] data; 
input busy; 
input clock; 
reg packet_valid; 
reg [7:0] data; 

integer delay;

// Define a task to generate packets and call the drive task
task gen_and_drive(input integer no_of_pkts);
integer i;
begin

  for(i=1;i<=no_of_pkts;i=i+1)
  begin
    delay={$random()}%4;

    $display("Time:%0d ns DRIVER gen and drive pkt_no = %d delay %d", $time, i,delay);

    repeat (delay)@(negedge clock);

    // randomize the packet
    gen.randomize();

    //pack the packet
    gen.packing();

    // call the drive packet task.
    @(negedge clock);
    drive_packet();
  end
end
endtask

task drive_packet() ;
  integer i;
  begin

    $display("Time:%0d ns DRIVER Starting to drive packet to port %0d len %0d ",$time,gen.Da,gen.len);
    repeat(4) @ (negedge clock);

    for (i=0;i<gen.len+4;i=i+1)
    begin
     @ (negedge clock);
     packet_valid = 1 ;
     data[7:0] = gen.pkt[i];
     $display("Time:%0d ns [DRIVER] data %b at i %d", $time, gen.pkt[i], i);
    end

    @ (negedge clock);
    packet_valid = 0;
    @ (negedge clock);

    end
    endtask

endmodule : driver