# Documentation

**LAB 02**  
**Name:** Alvi Akbar  
**NSID:** ala273  
**Student No:** 11118887

## Part 1

### 4)

* Packets are successfully captured by the monitor/receiver modules.

```text
# ******************* Start of testcases ****************
# 0 : Environment : start of run() method
# 0 : Environment : start of reset() method
# 17 : Environment : end of reset() method
# 17 : Environment : start of cfg_dut() method
# 80 : Environment : end of cfg_dut() method
# Time:80 ns DRIVER gen and drive pkt_no =           1 delay           1
# Time:90 ns [PACKING] pkt[0] is Da 00100100  36 Sa 00001101  13 len 00100110  38
# Time:100 ns DRIVER Starting to drive packet to port 36 len 38
# Time:150 ns [DRIVER] data 00100100 at i           0
# Time:160 ns [DRIVER] data 00001101 at i           1
# Time:170 ns [DRIVER] data 00100110 at i           2
# Time:180 ns [DRIVER] data 00000001 at i           3
# Time:190 ns [DRIVER] data 11101101 at i           4
# Time:200 ns RECV BYTE at PORT[  0] 00100100
# Time:200 ns [DRIVER] data 10001100 at i           5
# Time:210 ns [DRIVER] data 11111001 at i           6
# Time:210 ns RECV BYTE at PORT[  0] 00001101
# Time:220 ns RECV BYTE at PORT[  0] 00100110
# Time:220 ns [DRIVER] data 11000110 at i           7
# Time:230 ns [DRIVER] data 11000101 at i           8
# Time:230 ns RECV BYTE at PORT[  0] 00000001
# Time:240 ns RECV BYTE at PORT[  0] 11101101
# Time:240 ns [DRIVER] data 10101010 at i           9
# Time:250 ns [DRIVER] data 11100101 at i          10
# Time:250 ns RECV BYTE at PORT[  0] 10001100
# Time:260 ns RECV BYTE at PORT[  0] 11111001
# Time:260 ns [DRIVER] data 01110111 at i          11
# Time:270 ns [DRIVER] data 00010010 at i          12
```

### 5)

* **Bug 1:** generator.sv: 25 - wasn't referencing mem properly.

* **Bug 2:** driver.sv: 17 -  Driver the UID should starts from 1 to packet count. An extra packet was being generated.
  
### 8)

#### Test Plan

Feature | Testpoint | Stimulus/Mode of Operation | Expected Results
---------|----------|---------|---------
 **Port Mapping** |||
 | 8.1.1 | Test if `ready[x]` is asserted after packet arrives at `data` | Drive packet into `data` on valid packet signal `data_status` | `ready[x]` should be asserted when `mem_data` equals `[destination address byte]` of driven on the next clock cycle
 | | Test if incoming packet is routed to correct port | With `ready0`, `ready1`, `ready2`, or `ready3` asserted, drive series of valid packets into `data` | Incoming Packets in `data` should be forwarded to one of the four destination ports `port_x` based on packets destination address when `mem_data` equals `[destination address byte]` of driven on the next clock cycle
 **Packet Validity**|||
 |8.1.2| Test if the payload data in the each received packet is not corrupted by comparing it with the payload data in corresponding sent packet | Drive packet into `data` on valid packet signal | `data` should match the corrosponding packet from `port x`

### Test Results

``` text
# ******************* Start of testcases ****************
# 0 : Environment : start of run() method
# 0 : Environment : start of reset() method
# 20 : Environment : end of reset() method
# 20 : Environment : start of cfg_dut() method
# 20 : Environment : start of reset() method
# 40 : Environment : end of reset() method
# 180 : Environment : end of cfg_dut() method
# Time:180 ns DRIVER gen and drive pkt_no =           1 delay           1
# Time:190 ns [PACKING] pkt[0] is Da 00100100  36 Sa 00001101  13 len 00100110  38 
# Time:200 ns DRIVER Starting to drive packet to port 36 len 38 
# Time:250 ns [DRIVER] data 00100100 at i           0
# Time:260 ns [DRIVER] data 00001101 at i           1
# Time:270 ns [DRIVER] data 00100110 at i           2
# Time:280 ns [DRIVER] data 00000001 at i           3
# Time:290 ns [DRIVER] data 11101101 at i           4
# Time:300 ns RECV BYTE at PORT[  0] 00100100
# Time:300 ns [DRIVER] data 10001100 at i           5
# Time:310 ns [DRIVER] data 11111001 at i           6
# Time:310 ns RECV BYTE at PORT[  0] 00001101
# Time:320 ns RECV BYTE at PORT[  0] 00100110
# Time:320 ns [DRIVER] data 11000110 at i           7
# Time:330 ns [DRIVER] data 11000101 at i           8
# Time:330 ns RECV BYTE at PORT[  0] 00000001
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:340 ns RECV BYTE at PORT[  0] 11101101
# Time:340 ns [DRIVER] data 10101010 at i           9
# Time:350 ns [DRIVER] data 11100101 at i          10
# Time:350 ns RECV BYTE at PORT[  0] 10001100
# Time:360 ns RECV BYTE at PORT[  0] 11111001
# Time:360 ns [DRIVER] data 01110111 at i          11
# Time:370 ns [DRIVER] data 00010010 at i          12
# Time:370 ns RECV BYTE at PORT[  0] 11000110
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:380 ns RECV BYTE at PORT[  0] 11000101
# Time:380 ns [DRIVER] data 10001111 at i          13
# Time:390 ns [DRIVER] data 11110010 at i          14
# Time:390 ns RECV BYTE at PORT[  0] 10101010
# Time:400 ns RECV BYTE at PORT[  0] 11100101
# Time:400 ns [DRIVER] data 11001110 at i          15
# Time:410 ns [DRIVER] data 11101000 at i          16
# Time:410 ns RECV BYTE at PORT[  0] 01110111
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:420 ns RECV BYTE at PORT[  0] 00010010
# Time:420 ns [DRIVER] data 11000101 at i          17
# Time:430 ns [DRIVER] data 01011100 at i          18
# Time:430 ns RECV BYTE at PORT[  0] 10001111
# Time:440 ns RECV BYTE at PORT[  0] 11110010
# Time:440 ns [DRIVER] data 10111101 at i          19
# Time:450 ns [DRIVER] data 00101101 at i          20
# Time:450 ns RECV BYTE at PORT[  0] 11001110
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:460 ns RECV BYTE at PORT[  0] 11101000
# Time:460 ns [DRIVER] data 01100101 at i          21
# Time:470 ns [DRIVER] data 01100011 at i          22
# Time:470 ns RECV BYTE at PORT[  0] 11000101
# Time:480 ns RECV BYTE at PORT[  0] 01011100
# Time:480 ns [DRIVER] data 00001010 at i          23
# Time:490 ns [DRIVER] data 10000000 at i          24
# Time:490 ns RECV BYTE at PORT[  0] 10111101
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:500 ns RECV BYTE at PORT[  0] 00101101
# Time:500 ns [DRIVER] data 00100000 at i          25
# Time:510 ns [DRIVER] data 10101010 at i          26
# Time:510 ns RECV BYTE at PORT[  0] 01100101
# Time:520 ns RECV BYTE at PORT[  0] 01100011
# Time:520 ns [DRIVER] data 10011101 at i          27
# Time:530 ns [DRIVER] data 10010110 at i          28
# Time:530 ns RECV BYTE at PORT[  0] 00001010
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:540 ns RECV BYTE at PORT[  0] 10000000
# Time:540 ns [DRIVER] data 00010011 at i          29
# Time:550 ns [DRIVER] data 00001101 at i          30
# Time:550 ns RECV BYTE at PORT[  0] 00100000
# Time:560 ns RECV BYTE at PORT[  0] 10101010
# Time:560 ns [DRIVER] data 01010011 at i          31
# Time:570 ns [DRIVER] data 01101011 at i          32
# Time:570 ns RECV BYTE at PORT[  0] 10011101
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:580 ns RECV BYTE at PORT[  0] 10010110
# Time:580 ns [DRIVER] data 11010101 at i          33
# Time:590 ns [DRIVER] data 00000010 at i          34
# Time:590 ns RECV BYTE at PORT[  0] 00010011
# Time:600 ns RECV BYTE at PORT[  0] 00001101
# Time:600 ns [DRIVER] data 10101110 at i          35
# Time:610 ns [DRIVER] data 00011101 at i          36
# Time:610 ns RECV BYTE at PORT[  0] 01010011
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:620 ns RECV BYTE at PORT[  0] 01101011
# Time:620 ns [DRIVER] data 11001111 at i          37
# Time:630 ns [DRIVER] data 00100011 at i          38
# Time:630 ns RECV BYTE at PORT[  0] 11010101
# Time:640 ns RECV BYTE at PORT[  0] 00000010
# Time:640 ns [DRIVER] data 00001010 at i          39
# Time:650 ns [DRIVER] data 11001010 at i          40
# Time:650 ns RECV BYTE at PORT[  0] 10101110
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:660 ns RECV BYTE at PORT[  0] 00011101
# Time:660 ns [DRIVER] data 00111100 at i          41
# Time:670 ns RECV BYTE at PORT[  0] 11001111
# Time:680 ns RECV BYTE at PORT[  0] 00100011
# Time:680 ns DRIVER gen and drive pkt_no =           2 delay           0
# Time:680 ns [PACKING] pkt[0] is Da 10000001 129 Sa 10110110 182 len 00000110   6 
# Time:690 ns DRIVER Starting to drive packet to port 129 len 6 
# Time:690 ns RECV BYTE at PORT[  0] 00001010
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:700 ns RECV BYTE at PORT[  0] 11001010
# Time:710 ns RECV BYTE at PORT[  0] 00111100
# Time:720 ns RECV BYTE at PORT[  0] 00111100
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:740 ns [DRIVER] data 10000001 at i           0
# Time:750 ns [DRIVER] data 10110110 at i           1
# Time:760 ns [DRIVER] data 00000110 at i           2
# Time:770 ns [DRIVER] data 00000010 at i           3
# Time:780 ns [DRIVER] data 10111100 at i           4
# Time:790 ns RECV BYTE at PORT[  1] 10000001
# Time:790 ns [DRIVER] data 00101010 at i           5
# Time:800 ns [DRIVER] data 00001011 at i           6
# Time:800 ns RECV BYTE at PORT[  1] 10110110
# Time:810 ns RECV BYTE at PORT[  1] 00000110
# Time:810 ns [DRIVER] data 01110001 at i           7
# Time:820 ns [DRIVER] data 10000101 at i           8
# Time:820 ns RECV BYTE at PORT[  1] 00000010
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 1 and valid
# 
# Time:830 ns RECV BYTE at PORT[  1] 10111100
# Time:830 ns [DRIVER] data 01001111 at i           9
# Time:840 ns RECV BYTE at PORT[  1] 00101010
# Time:850 ns RECV BYTE at PORT[  1] 00001011
# Time:850 ns DRIVER gen and drive pkt_no =           3 delay           1
# Time:860 ns [PACKING] pkt[0] is Da 00100100  36 Sa 01001100  76 len 00100111  39 
# Time:860 ns RECV BYTE at PORT[  1] 01110001
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 1 and valid
# 
# Time:870 ns RECV BYTE at PORT[  1] 10000101
# Time:870 ns DRIVER Starting to drive packet to port 36 len 39 
# Time:880 ns RECV BYTE at PORT[  1] 01001111
# Time:890 ns RECV BYTE at PORT[  1] 01001111
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 1 and valid
# 
# Time:920 ns [DRIVER] data 00100100 at i           0
# Time:930 ns [DRIVER] data 01001100 at i           1
# Time:940 ns [DRIVER] data 00100111 at i           2
# Time:950 ns [DRIVER] data 00000011 at i           3
# Time:960 ns [DRIVER] data 11111000 at i           4
# Time:960 ns RECV BYTE at PORT[  0] 00100100
# Time:970 ns RECV BYTE at PORT[  0] 01001100
# Time:970 ns [DRIVER] data 10110111 at i           5
# Time:980 ns [DRIVER] data 10011111 at i           6
# Time:980 ns RECV BYTE at PORT[  0] 00100111
# Time:990 ns RECV BYTE at PORT[  0] 00000011
# Time:990 ns [DRIVER] data 01011100 at i           7
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1000 ns [DRIVER] data 01011011 at i           8
# Time:1000 ns RECV BYTE at PORT[  0] 11111000
# Time:1010 ns RECV BYTE at PORT[  0] 10110111
# Time:1010 ns [DRIVER] data 10001001 at i           9
# Time:1020 ns [DRIVER] data 01001001 at i          10
# Time:1020 ns RECV BYTE at PORT[  0] 10011111
# Time:1030 ns RECV BYTE at PORT[  0] 01011100
# Time:1030 ns [DRIVER] data 11010000 at i          11
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1040 ns [DRIVER] data 11010111 at i          12
# Time:1040 ns RECV BYTE at PORT[  0] 01011011
# Time:1050 ns RECV BYTE at PORT[  0] 10001001
# Time:1050 ns [DRIVER] data 01010001 at i          13
# Time:1060 ns [DRIVER] data 10010110 at i          14
# Time:1060 ns RECV BYTE at PORT[  0] 01001001
# Time:1070 ns RECV BYTE at PORT[  0] 11010000
# Time:1070 ns [DRIVER] data 00001100 at i          15
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1080 ns [DRIVER] data 11000010 at i          16
# Time:1080 ns RECV BYTE at PORT[  0] 11010111
# Time:1090 ns RECV BYTE at PORT[  0] 01010001
# Time:1090 ns [DRIVER] data 11001000 at i          17
# Time:1100 ns [DRIVER] data 01110111 at i          18
# Time:1100 ns RECV BYTE at PORT[  0] 10010110
# Time:1110 ns RECV BYTE at PORT[  0] 00001100
# Time:1110 ns [DRIVER] data 00111101 at i          19
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1120 ns [DRIVER] data 00010010 at i          20
# Time:1120 ns RECV BYTE at PORT[  0] 11000010
# Time:1130 ns RECV BYTE at PORT[  0] 11001000
# Time:1130 ns [DRIVER] data 01111110 at i          21
# Time:1140 ns [DRIVER] data 01101101 at i          22
# Time:1140 ns RECV BYTE at PORT[  0] 01110111
# Time:1150 ns RECV BYTE at PORT[  0] 00111101
# Time:1150 ns [DRIVER] data 00111001 at i          23
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1160 ns [DRIVER] data 00011111 at i          24
# Time:1160 ns RECV BYTE at PORT[  0] 00010010
# Time:1170 ns RECV BYTE at PORT[  0] 01111110
# Time:1170 ns [DRIVER] data 11010011 at i          25
# Time:1180 ns [DRIVER] data 10000101 at i          26
# Time:1180 ns RECV BYTE at PORT[  0] 01101101
# Time:1190 ns RECV BYTE at PORT[  0] 00111001
# Time:1190 ns [DRIVER] data 01111000 at i          27
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1200 ns [DRIVER] data 01011011 at i          28
# Time:1200 ns RECV BYTE at PORT[  0] 00011111
# Time:1210 ns RECV BYTE at PORT[  0] 11010011
# Time:1210 ns [DRIVER] data 01001001 at i          29
# Time:1220 ns [DRIVER] data 00111111 at i          30
# Time:1220 ns RECV BYTE at PORT[  0] 10000101
# Time:1230 ns RECV BYTE at PORT[  0] 01111000
# Time:1230 ns [DRIVER] data 00101010 at i          31
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1240 ns [DRIVER] data 01011000 at i          32
# Time:1240 ns RECV BYTE at PORT[  0] 01011011
# Time:1250 ns RECV BYTE at PORT[  0] 01001001
# Time:1250 ns [DRIVER] data 10000110 at i          33
# Time:1260 ns [DRIVER] data 10001110 at i          34
# Time:1260 ns RECV BYTE at PORT[  0] 00111111
# Time:1270 ns RECV BYTE at PORT[  0] 00101010
# Time:1270 ns [DRIVER] data 10011100 at i          35
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1280 ns [DRIVER] data 11111010 at i          36
# Time:1280 ns RECV BYTE at PORT[  0] 01011000
# Time:1290 ns RECV BYTE at PORT[  0] 10000110
# Time:1290 ns [DRIVER] data 00100110 at i          37
# Time:1300 ns [DRIVER] data 01110011 at i          38
# Time:1300 ns RECV BYTE at PORT[  0] 10001110
# Time:1310 ns RECV BYTE at PORT[  0] 10011100
# Time:1310 ns [DRIVER] data 10100011 at i          39
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1320 ns [DRIVER] data 00101111 at i          40
# Time:1320 ns RECV BYTE at PORT[  0] 11111010
# Time:1330 ns RECV BYTE at PORT[  0] 00100110
# Time:1330 ns [DRIVER] data 10110011 at i          41
# Time:1340 ns [DRIVER] data 01011111 at i          42
# Time:1340 ns RECV BYTE at PORT[  0] 01110011
# Time:1350 ns RECV BYTE at PORT[  0] 10100011
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1360 ns DRIVER gen and drive pkt_no =           4 delay           2
# Time:1360 ns RECV BYTE at PORT[  0] 00101111
# Time:1370 ns RECV BYTE at PORT[  0] 10110011
# Time:1380 ns [PACKING] pkt[0] is Da 00001001   9 Sa 11011010 218 len 00100011  35 
# Time:1380 ns RECV BYTE at PORT[  0] 01011111
# Time:1390 ns RECV BYTE at PORT[  0] 01011111
# Time:1390 ns DRIVER Starting to drive packet to port 9 len 35 
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 0 and val
# 
# Time:1440 ns [DRIVER] data 00001001 at i           0
# Time:1450 ns [DRIVER] data 11011010 at i           1
# Time:1460 ns [DRIVER] data 00100011 at i           2
# Time:1470 ns [DRIVER] data 00000100 at i           3
# Time:1480 ns [DRIVER] data 11011111 at i           4
# Time:1480 ns RECV BYTE at PORT[  2] 00001001
# Time:1490 ns RECV BYTE at PORT[  2] 11011010
# Time:1490 ns [DRIVER] data 01111001 at i           5
# Time:1500 ns [DRIVER] data 01000100 at i           6
# Time:1500 ns RECV BYTE at PORT[  2] 00100011
# Time:1510 ns RECV BYTE at PORT[  2] 00000100
# Time:1510 ns [DRIVER] data 11010000 at i           7
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1520 ns [DRIVER] data 00101010 at i           8
# Time:1520 ns RECV BYTE at PORT[  2] 11011111
# Time:1530 ns RECV BYTE at PORT[  2] 01111001
# Time:1530 ns [DRIVER] data 10101011 at i           9
# Time:1540 ns [DRIVER] data 00001110 at i          10
# Time:1540 ns RECV BYTE at PORT[  2] 01000100
# Time:1550 ns RECV BYTE at PORT[  2] 11010000
# Time:1550 ns [DRIVER] data 11011100 at i          11
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1560 ns [DRIVER] data 10011010 at i          12
# Time:1560 ns RECV BYTE at PORT[  2] 00101010
# Time:1570 ns RECV BYTE at PORT[  2] 10101011
# Time:1570 ns [DRIVER] data 11111101 at i          13
# Time:1580 ns [DRIVER] data 11000011 at i          14
# Time:1580 ns RECV BYTE at PORT[  2] 00001110
# Time:1590 ns RECV BYTE at PORT[  2] 11011100
# Time:1590 ns [DRIVER] data 01010110 at i          15
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1600 ns [DRIVER] data 01001110 at i          16
# Time:1600 ns RECV BYTE at PORT[  2] 10011010
# Time:1610 ns RECV BYTE at PORT[  2] 11111101
# Time:1610 ns [DRIVER] data 01100111 at i          17
# Time:1620 ns [DRIVER] data 00001010 at i          18
# Time:1620 ns RECV BYTE at PORT[  2] 11000011
# Time:1630 ns RECV BYTE at PORT[  2] 01010110
# Time:1630 ns [DRIVER] data 10110110 at i          19
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1640 ns [DRIVER] data 00111000 at i          20
# Time:1640 ns RECV BYTE at PORT[  2] 01001110
# Time:1650 ns RECV BYTE at PORT[  2] 01100111
# Time:1650 ns [DRIVER] data 01111001 at i          21
# Time:1660 ns [DRIVER] data 10111000 at i          22
# Time:1660 ns RECV BYTE at PORT[  2] 00001010
# Time:1670 ns RECV BYTE at PORT[  2] 10110110
# Time:1670 ns [DRIVER] data 10010100 at i          23
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1680 ns [DRIVER] data 10010011 at i          24
# Time:1680 ns RECV BYTE at PORT[  2] 00111000
# Time:1690 ns RECV BYTE at PORT[  2] 01111001
# Time:1690 ns [DRIVER] data 00000100 at i          25
# Time:1700 ns [DRIVER] data 01011001 at i          26
# Time:1700 ns RECV BYTE at PORT[  2] 10111000
# Time:1710 ns RECV BYTE at PORT[  2] 10010100
# Time:1710 ns [DRIVER] data 11011011 at i          27
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1720 ns [DRIVER] data 01001101 at i          28
# Time:1720 ns RECV BYTE at PORT[  2] 10010011
# Time:1730 ns RECV BYTE at PORT[  2] 00000100
# Time:1730 ns [DRIVER] data 11011001 at i          29
# Time:1740 ns [DRIVER] data 01101101 at i          30
# Time:1740 ns RECV BYTE at PORT[  2] 01011001
# Time:1750 ns RECV BYTE at PORT[  2] 11011011
# Time:1750 ns [DRIVER] data 01110110 at i          31
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1760 ns [DRIVER] data 11001010 at i          32
# Time:1760 ns RECV BYTE at PORT[  2] 01001101
# Time:1770 ns RECV BYTE at PORT[  2] 11011001
# Time:1770 ns [DRIVER] data 10110110 at i          33
# Time:1780 ns [DRIVER] data 10010101 at i          34
# Time:1780 ns RECV BYTE at PORT[  2] 01101101
# Time:1790 ns RECV BYTE at PORT[  2] 01110110
# Time:1790 ns [DRIVER] data 01000110 at i          35
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1800 ns [DRIVER] data 00000100 at i          36
# Time:1800 ns RECV BYTE at PORT[  2] 11001010
# Time:1810 ns RECV BYTE at PORT[  2] 10110110
# Time:1810 ns [DRIVER] data 11110111 at i          37
# Time:1820 ns [DRIVER] data 01101001 at i          38
# Time:1820 ns RECV BYTE at PORT[  2] 10010101
# Time:1830 ns RECV BYTE at PORT[  2] 01000110
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
# Time:1840 ns RECV BYTE at PORT[  2] 00000100
# Time:1850 ns RECV BYTE at PORT[  2] 11110111
# Time:1860 ns RECV BYTE at PORT[  2] 01101001
# Time:1870 ns RECV BYTE at PORT[  2] 01101001
# 
# 
# TEST 8.1.2: Port Mapping and Valid Packet
# 
# 
# Test if incoming packet is routed to correct port without being corrupted
# 
# Success: packet forwarded to port 2 and valid
# 
############# TEST PASSED #############
```
