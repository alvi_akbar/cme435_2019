`include "testbench/utils/data_types.sv"

`ifndef INTERFACE_SV
`define INTERFACE_SV

interface intf(input logic clk, input logic rst_b);
    out_data_t data_out_1,
               data_out_2,
               data_out_3,
               data_out_4;

    new_len_t newdata_len_1,
              newdata_len_2,
              newdata_len_3,
              newdata_len_4;

    in_data_t data_in;

    logic proceed_1,
          proceed_2,
          proceed_3,
          proceed_4,
          bnd_plse,
          ack;


    event error;
    integer error_count;

    clocking cbDriv @(posedge clk);
        output bnd_plse;
        output data_in;
        input ack;
    endclocking

    clocking cbMon @(posedge clk);
        output proceed_1;
        output proceed_2;
        output proceed_3;
        output proceed_4;
        input ack;
        input newdata_len_1;
        input newdata_len_2;
        input newdata_len_3;
        input newdata_len_4;
        input data_out_1;
        input data_out_2;
        input data_out_3;
        input data_out_4;
    endclocking


    modport DUT(
        input bnd_plse, proceed_1, proceed_2, proceed_3, proceed_4, data_in, clk, rst_b,
        output ack, newdata_len_1, newdata_len_2, newdata_len_3, newdata_len_4, data_out_1,
               data_out_2, data_out_3, data_out_4
    );

    modport DRIVER(
        clocking cbDriv,
        input rst_b
    );

    modport MONITOR(
        clocking cbMon,
        input rst_b
    );

endinterface : intf
`endif