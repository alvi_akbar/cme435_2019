`timescale 1ns/1ns

`include "dut/dut_top.sv"
`include "testbench/phase1_top/interface.sv"
`include "testbench/phase9_testcases/test_sanity_check.sv"
`include "testbench/phase9_testcases/test_reset.sv"
`include "testbench/phase9_testcases/test_bnd_plse.sv"

module tbench_top;
    /* clock and rst_b signal declaration */
    logic clk, rst_b;

    /* clock generation */
    always #5 clk = ~clk;

    /* rst_b Generation */
    initial begin
        clk = 1;

        /* Initial Reset */

        i_intf.error_count = 0;
        rst_b = 0;

        repeat(2) @(posedge clk);

        rst_b = 1;
    end

    /* Increment the error counter when error occurs. */
    always @(i_intf.error) begin
        #0 i_intf.error_count += 1;
        $display("Time:%0d ns ERROR RECEIVED", $time);
    end

    intf i_intf (clk, rst_b);

    /************************************ Start TestCases ************************************/
    testbench testcases (i_intf);
    /************************************ End TestCases *************************************/

    /* DUT instance, interface signals are connected to the DUT ports */
    dut_top dut(i_intf.DUT);

    initial begin
        $dumpfile("dump.vcd");
        $dumpvars; /* enabling the wave dump */
    end

endmodule: tbench_top