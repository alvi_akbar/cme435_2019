`include "testbench/phase1_top/interface.sv"
`include "testbench/phase3_base/transaction.sv"

`ifndef DRIVER_SV
`define DRIVER_SV

class driver;
    virtual intf vif;

    int no_transactions;
    transaction trans;

    in_data_t payload [$];

    mailbox drv2scb;
    mailbox gen2drv;

    function new (virtual intf vif, mailbox gen2drv, drv2scb);
        this.vif = vif;

        this.gen2drv = gen2drv;
        this.drv2scb = drv2scb;
    endfunction : new

    task main;
        forever begin
            gen2drv.get(trans);

            @vif.cbDriv;
            payload = trans.pkt.payload;

            /* First Payload Byte */
            @vif.cbDriv;
            vif.bnd_plse = 1;
            
            trans.bnd_plse = vif.bnd_plse;
            trans.display("[ DRIVER ]");

            vif.data_in = payload.pop_front;

            foreach (payload[i]) begin
                @vif.cbDriv;
                vif.data_in = payload[i];
                vif.bnd_plse = i == payload.size()-1; /* Last Payload Byte */
            end

            @vif.cbDriv;
            vif.bnd_plse = 0;
            trans.bnd_plse = vif.bnd_plse;
            vif.data_in = 8'h0;

            trans.display("[ DRIVER ]");

            no_transactions++;

            wait (vif.ack); /* wait until no_transaction if awk is high */
            void'(trans.pkt.payload.pop_front());

            drv2scb.put(trans); /* Sending data to Scoreboard */
        end

    endtask : main

endclass : driver

`endif