`include "testbench/utils/data_types.sv"
`include "testbench/phase1_top/interface.sv"
`include "testbench/phase3_base/transaction.sv"

`ifndef MONITOR_SV
`define MONITOR_SV

class monitor;

    virtual intf vif;
    transaction trans;

    mailbox mon2scb;

    function new (virtual intf vif, mailbox mon2scb);
        this.vif = vif;
        this.mon2scb = mon2scb;
    endfunction

    task main;
        forever begin
           trans = new ();

            @vif.cbMon;
            wait (vif.cbMon.newdata_len_1
                || vif.cbMon.newdata_len_2
                || vif.cbMon.newdata_len_3
                || vif.cbMon.newdata_len_4);

            trans.newdata_len = {vif.cbMon.newdata_len_1, vif.cbMon.newdata_len_2, vif.cbMon.newdata_len_3, 
                vif.cbMon.newdata_len_4};

            vif.proceed_1 = vif.cbMon.newdata_len_1 != 0;
            vif.proceed_2 = vif.cbMon.newdata_len_2 != 0;
            vif.proceed_3 = vif.cbMon.newdata_len_3 != 0;
            vif.proceed_4 = vif.cbMon.newdata_len_4 != 0;

            trans.proceed = '{vif.cbMon.proceed_1, vif.cbMon.proceed_2, vif.cbMon.proceed_3,
                vif.cbMon.proceed_4};

            foreach ( trans.proceed[i] ) 
                if ( trans.proceed[i] == 1 )
                    trans.pkt.dst_addr = i + 1;

            @vif.cbMon;
            {vif.proceed_1, vif.proceed_2, vif.proceed_3, vif.proceed_4} = '{4{0}};

            trans.proceed = '{vif.cbMon.proceed_1, vif.cbMon.proceed_2, vif.cbMon.proceed_3,
                vif.cbMon.proceed_4};
                
            @vif.cbMon;
            repeat (trans.newdata_len.max()[0]+1) begin
                trans.data_out = vif.cbMon.data_out_1
                    + vif.cbMon.data_out_2
                    + vif.cbMon.data_out_3
                    + vif.cbMon.data_out_4;

                @vif.cbMon;
                if (trans.data_out)
                    trans.pkt.payload.push_back(trans.data_out);
            end

            trans.display("[ MONITOR ]");

            @vif.cbMon;
            mon2scb.put(trans);
        end

    endtask

endclass : monitor

`endif