`include "testbench/utils/data_types.sv"
`include "testbench/phase3_base/transaction.sv"

`ifndef SCOREBOARD_SV
`define SCOREBOARD_SV

class scoreboard;
    virtual intf i_intf;
    int no_transactions;

    transaction trans_actual;   /* from monitor */
    transaction trans_expected; /* from driver */


    mailbox drv2scb;
    mailbox mon2scb;

    transaction mem_expected [$]; /* from driver to scoreboard */
    transaction mem_actual [$]; /* from monitor to scoreboard */

    /************************* CONSTRUCTORS ****************************/

    function new (virtual intf i_intf, mailbox drv2scb, mon2scb);
        this.i_intf = i_intf;

        /* getting the mailbox handles from environment */
        this.drv2scb = drv2scb;
        this.mon2scb = mon2scb;

        this.no_transactions = 0;

        mem_actual = {};
        mem_expected = {};

    endfunction : new

    /********************** METHODS & TASKS *****************************/

    /* Compares the actual result with the expected result */
    task main();
        forever begin
            drv2scb.get(trans_expected);
            mon2scb.get(trans_actual);

            mem_expected.push_back(trans_expected);
            mem_actual.push_back(trans_actual);

            $display(">>> TESTCASE: Test Sanity Check >>> \n");

            foreach (mem_actual[i]) begin

                $display("---------------------------------- TRANSACTION NO: %0d -----------------------------",
                    mem_actual[i].id);


                $display("------------------------------------------------------------------------------------");
                $display("%t : TEST 1 : Test Correct Transaction ID", $time);
                $display("------------------------------------------------------------------------------------");

                test_correct_transaction_id: assert (mem_actual[i].id == mem_expected[i].id)
                    $display("%t : scoreboard: Assertion test_correct_transaction_id Successful!", $time);
                    else begin
                        ->i_intf.error;
                        $error("%t : scoreboard : Wrong  Result!\n\t Expected:  %0d  Actual:  %0d", $time,
                        mem_expected[i].id, mem_actual[i].id);
                    end

                $display("------------------------------------------------------------------------------------");
                $display("%t : TEST 2 : Test Correct Payload Size", $time);
                $display("------------------------------------------------------------------------------------");

                foreach (mem_actual[i])
                    test_correct_payload_size: assert (mem_actual[i].pkt.payload.size() == mem_expected[i].pkt.payload.size())
                        else begin
                            ->i_intf.error;
                            $error("%t : scoreboard : Wrong  Result!\n\t Expected:  %0d  Actual:  %0d", $time,
                            mem_expected[i].pkt.payload.size(), mem_actual[i].pkt.payload.size());
                        end

                $display("%t scoreboard : Assertion test_correct_payload_size Successful!", $time);


                $display("------------------------------------------------------------------------------------");
                $display("%t : TEST 3 : Test Correct Payload Data", $time);
                $display("------------------------------------------------------------------------------------");

                foreach (mem_actual[i].pkt.payload[j])
                    test_correct_payload_data: assert (mem_actual[i].pkt.payload[j] == mem_expected[i].pkt.payload[j])
                        else begin
                            ->i_intf.error;
                            $error("%t : scoreboard : Wrong  Result!\n\t Expected:  %0d  Actual:  %0d", $time,
                            mem_expected[i].pkt.payload[j], mem_actual[i].pkt.payload[j]);
                        end
                $display("%t scoreboard : Assertion test_correct_payload_data Successful!", $time);

            end
            
            $display("<<< TESTCASE: Test Sanity Check <<<");
            no_transactions++;

            if (i_intf.error_count > 0)
                $fatal("ERRORS DETECTED");

            trans_actual.display("[ SCOREBOARD ]");
        end
    endtask : main

endclass : scoreboard
`endif
