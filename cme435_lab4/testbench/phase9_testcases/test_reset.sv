`timescale 1ns/1ns

`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"

program testbench(
    intf i_intf
);

    /* declaring environment instance */
    environment env;

    task test_reset(environment env);
        wait(i_intf.rst_b == 0);

        
        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 1 : Test Reset Assert Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_reset_assert_low: assert (i_intf.rst_b == 0)
            $display("%t : test_reset: Assertion test_reset_assert_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.rst_b, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 2 : Test All Data Signals Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_data_out_1_low: assert (i_intf.data_out_1 == 0)
            $display("%t : test_reset: Assertion test_data_out_1_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.data_out_1, 0);
            end

        test_data_out_2_low: assert (i_intf.data_out_2 == 0)
            $display("%t : test_reset: Assertion test_data_out_2_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.data_out_2, 0);
            end

        test_data_out_3_low: assert (i_intf.data_out_3 == 0)
            $display("%t : test_reset: Assertion test_data_out_3_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.data_out_3, 0);
            end

        test_data_out_4_low: assert (i_intf.data_out_4 == 0)
            $display("%t : test_reset: Assertion test_data_out_4_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.data_out_4, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 3 : Test All Proceed Signals Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_proceed_1_low: assert (i_intf.proceed_1 == 0)
            $display("%t : test_reset: Assertion test_proceed_1_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.proceed_1, 0);
            end

        test_proceed_2_low: assert (i_intf.proceed_2 == 0)
            $display("%t : test_reset: Assertion test_proceed_2_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.proceed_2, 0);
            end

        test_proceed_3_low: assert (i_intf.proceed_3 == 0)
            $display("%t : test_reset: Assertion test_proceed_3_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.proceed_3, 0);
            end

        test_proceed_4_low: assert (i_intf.proceed_4 == 0)
            $display("%t : test_reset: Assertion test_proceed_4_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.proceed_4, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 4 : Test All New Data Len Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_newdata_len_1_low: assert (i_intf.newdata_len_1 == 0)
            $display("%t : test_reset: Assertion test_newdata_len_1_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.newdata_len_1, 0);
            end

        test_newdata_len_2_low: assert (i_intf.newdata_len_2 == 0)
            $display("%t : test_reset: Assertion test_newdata_len_2_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.newdata_len_2, 0);
            end

        test_newdata_len_3_low: assert (i_intf.newdata_len_3 == 0)
            $display("%t : test_reset: Assertion test_newdata_len_3_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.newdata_len_3, 0);
            end

        test_newdata_len_4_low: assert (i_intf.newdata_len_4 == 0)
            $display("%t : test_reset: Assertion test_newdata_len_4_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.newdata_len_4, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 6 : Test BND PLSE Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_bnd_plse_low: assert (i_intf.bnd_plse == 0)
            $display("%t : test_reset: Assertion test_bnd_plse_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.bnd_plse, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 7 : Test ACK Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_ack_low: assert (i_intf.ack == 0)
            $display("%t : test_reset: Assertion test_ack_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.ack, 0);
            end

        $display("------------------------------------------------------------------------------------");
        $display("%t : RESET TEST 8 : Test Data Input Low", $time);
        $display("------------------------------------------------------------------------------------");
        test_data_in_low: assert (i_intf.data_in == 0)
            $display("%t : test_reset: Assertion test_data_in_low Successful!", $time);
            else begin
                ->i_intf.error;
                $error("%t : test_reset : Wrong  Result!\n\t Actual:  %0d  Expected:  %0d", $time,
                i_intf.data_in, 0);
            end

    endtask : test_reset

    initial begin
        /* creating environment */
        env = new (i_intf);

        /* setting the repeat count of generator such as 5, means to generate 5 packets */
        env.gen.repeat_count = 10;

        $display("******************* Start of TESTCASE_RESET ****************");

        fork
            env.run();
            begin
                #200
                repeat(env.gen.repeat_count) begin
                    @(posedge i_intf.clk);
                    test_reset(env);
                end
            end

            begin
                #200
                repeat(5) begin
                    repeat (3) @i_intf.clk;
                    env.tbench_top.rst_b = 0;
                    repeat (5) @i_intf.clk;
                    env.tbench_top.rst_b = 1;
                end
            end
        join
    end

    final
        $display("******************* End of TESTCASE_RESET ****************");

endprogram : testbench