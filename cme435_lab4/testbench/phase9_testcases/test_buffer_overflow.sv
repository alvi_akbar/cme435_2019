`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"
`include "testbench/phase3_base/transaction.sv"

class mock_pkt_c extends pkt_t;

    constraint payload_size_c{
        payload.size() inside {13, 17};
    }

    // constraint dst_addr_c{
        // dst_addr == 1;
    // }
    // constraint order_c{
        // solve dst_addr before payload;
    // }
endclass : mock_pkt_c

class mock_transaction_c extends transaction;

    mock_pkt_c mock_pkt;

    function new ();
        mock_pkt = new ();
        pkt = mock_pkt; /* Polymorphism */
    endfunction : new
endclass : mock_transaction_c

program testbench(
    intf i_intf
);

    /* declaring environment instance */
    environment #(mock_transaction_c) env;
    
    initial begin

        /* creating environment */
        env = new (i_intf);

        /* setting the repeat count of generator such as 5, means to generate 5 packets */
        /* TODO: Verify how to handle the over flow - overflow should happen as its 4 bit 
        and hence the largest number of repeat count value should be 16*/
        env.gen.repeat_count = 20;

        $display("******************* Start of TESTCASE_BUFFER_OVERFLOW ****************");
        
        /* calling run of env, it in turns calls other main tasks */
        env.run();
    end

    final
        $display("******************* End of testcase_buffer_overflow ****************");
            
endprogram : testbench