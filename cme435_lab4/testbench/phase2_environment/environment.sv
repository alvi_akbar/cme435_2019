`include "testbench/utils/data_types.sv"
`include "testbench/phase1_top/interface.sv"
`include "testbench/phase3_base/transaction.sv"
`include "testbench/phase4_generator/generator.sv"
`include "testbench/phase5_driver/driver.sv"
`include "testbench/phase6_monitor/monitor.sv"
`include "testbench/phase7_scoreboard/scoreboard.sv"

`ifndef ENVIRONMENT_SV
`define ENVIRONMENT_SV

class environment #(type T = transaction);
    generator #(T) gen;
    driver driv;
    monitor mon;
    scoreboard scb;

    mailbox gen2driv;
    mailbox drv2scb;
    mailbox mon2scb;

    virtual intf vif;

    function new (virtual intf vif);

        this.vif = vif;

        $display("%0d : Environment : created env object", $time);

        gen2driv = new ();
        drv2scb = new ();
        mon2scb = new ();

        /* Upsteam Components */
        gen = new (vif, gen2driv);
        driv = new (vif, gen2driv, drv2scb);
        
        /* Downstream Components */
        mon = new (vif, mon2scb);
        scb = new (vif, drv2scb, mon2scb);

    endfunction

    task pre_test();
        $display("%0d : Environment : start of pre_test()", $time);
        reset();
        $display("%0d : Environment : end of pre_test()", $time);
    endtask : pre_test

    task test();
        $display("%0d : Environment : start of test()", $time);

        fork
            gen.main();
            driv.main();
            mon.main();
            scb.main();
        join_any

        wait (gen.ended.triggered); /* optional */
        wait (gen.repeat_count == driv.no_transactions);
        wait (gen.repeat_count == scb.no_transactions);
        
        $display("%0d : Environment : end of test()", $time);
    endtask : test

    task post_test();
        $display("%0d : Environment : start of post_test()", $time);

        if (vif.error_count != 0)
            $display("############# TEST FAILED #############");
        else
            $display("############# TEST PASSED #############");

        $display("%0d : Environment : end of post_test()", $time);

    endtask : post_test

    task reset();
        wait (!vif.rst_b);
        $display("[ ENVIRONMENT ] ----- Reset Started -----");

        vif.error_count = 0;

        vif.proceed_1 = 1'b0;
        vif.proceed_2 = 1'b0;
        vif.proceed_3 = 1'b0;
        vif.proceed_4 = 1'b0;

        vif.bnd_plse = 1'b0;
        vif.data_in = 8'h00;

        wait (vif.rst_b);

        $display("[ ENVIRONMENT ] ----- Reset Ended -------");
    endtask : reset

    task run();
        $display("%0d : Environment : start of run()", $time);
        pre_test();
        test();
        post_test();
        $display("%0d : Environment : end of run()", $time);
    endtask : run

endclass : environment
`endif