`ifndef DATA_TYPES
   `define DATA_TYPES
   `include "testbench/data_types.sv"
`endif

`ifndef TRANSACTION
  `define TRANSACTION
  `include "testbench/transaction.sv"
`endif

class generator;
  //declaring transaction class
  rand transaction trans;

  // creating virtual interface handle
  virtual input_intf.IP vif;
  
  mailbox gen2driv;

  // repeat count, to specify number of items to generate
  int repeat_count;

  //event, to indicate the end of transaction generation
  event ended;

  in_data_t payload[$];

  //constructor
  function new(virtual input_intf.IP vif, mailbox gen2driv);
    // getting the interface
    this.vif = vif;
    // getting the mailbox handle from env.
    this.gen2driv = gen2driv;
  endfunction

  //main task, generates (creates and randomizes) transaction packets
  task main();
    repeat(repeat_count) begin
      trans = new();

      @(posedge vif.clk);
      if( !trans.randomize() )
        $fatal("Gen:: trans randomization failed");
      
      payload = trans.pkt.payload; // copying src queue to dst queue

      trans.bnd_plse = 1'b1;
      trans.data_in = trans.pkt.dst_addr;
      gen2driv.put(trans);

      @(negedge vif.clk);
      trans.bnd_plse = 1'b0;
      trans.data_in = payload[0];
      gen2driv.put(trans);

      $display("dst_addr: %0d", trans.pkt.dst_addr);

      payload = payload[1:$];

      foreach(payload[i]) begin
        @(posedge vif.clk);
        trans.data_in = payload[i];
        gen2driv.put(trans); 
        $display("DATAIN: %h", trans.data_in);
      end
            
      trans.display("[ Generator ]");
    end
    -> ended; //trigger end of generation
  endtask

endclass