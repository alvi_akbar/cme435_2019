`timescale 1ns/1ns

`ifndef INTERFACE
  `define INTERFACE 
  `include "testbench/interface.sv"
`endif

`ifndef TESTBENCH
  `define TESTBENCH  
  `include "testbench/testbench.sv"
`endif

module tbench_top;
    
    //clock and rst_b signal declaration
  logic clk, rst_b;
  
  event error;
  integer error_count;

  //clock generation
  always #5 clk = ~clk;
  
  //rst_b Generation
  initial begin
    clk = 1;
    error_count = 0;

    // Initial Reset
    rst_b = 0;
    repeat (2) begin
      @(posedge clk);
    end

    rst_b = 1;

  end

  // Increment the error counter when error occurs.
  always @(error)
  begin
    #0 error_count += 1;
    $display("Time:%0d ns ERROR RECEIVED", $time);
  end

  mon_intf i_mon_intf (clk, rst_b); // Instantiating output_intf
  input_intf i_in_intf (clk, rst_b); // Instantiating input_intf
  output_intf i_out_intf (clk, rst_b); // Instantiating output_intf
  
  testbench test (i_mon_intf.MON, i_in_intf.IP, i_out_intf.OP);  // Testcase instance, interface handle is passed to test as an argument
  
  dut_top dut (i_mon_intf, i_in_intf, i_out_intf); // DUT instance, interface signals are connected to the DUT ports

  initial begin
    $dumpfile("dump.vcd"); $dumpvars; //enabling the wave dump
  end
    
endmodule
