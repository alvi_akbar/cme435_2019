`ifndef TRANSACTION
  `define TRANSACTION
  `include "testbench/transaction.sv"
`endif

class driver;
  //creating virtual interface handle
  virtual input_intf.IP vif;

  //used to count the number of transactions
  int no_transactions;
  transaction trans;

  //creating mailbox handle
  mailbox gen2driv;


  //constructor
  function new(virtual input_intf.IP vif, mailbox gen2driv);
    //getting the interface
    this.vif = vif;

    //getting the mailbox handles from environment
    this.gen2driv = gen2driv;

  endfunction

  //drive the transaction items to DUT via interface signals
  task main();
    forever begin
      gen2driv.get(trans);

      vif.cb.data_in <= trans.data_in;

      vif.cb.bnd_plse <= trans.bnd_plse;

      trans.display("[ Driver ]");

      // Only increase the no_transaction if awk is high
      if(vif.cb.ack)
        no_transactions++;
    end

  endtask

endclass
