`ifndef INTERFACE
  `define INTERFACE 
  `include "testbench/interface.sv"
`endif

`ifndef ENVIRONMENT
  `define ENVIRONMENT
  `include "testbench/environment.sv" 
`endif

program testbench(
  mon_intf i_mon_intf,
  input_intf i_input_intf,
  output_intf i_output_intf
);

//declaring environment instance
  environment env;

  default clocking test_cb @(posedge i_input_intf.clk);
  endclocking

  initial begin
    //creating environment
    env = new(i_mon_intf, i_input_intf, i_output_intf); 
    
    //setting the repeat count of generator such as 5, means to generate 5 packets 
    env.gen.repeat_count = 10;

    $display("******************* Start of testcase ****************");
    //calling run of env, it in turns calls other main tasks.
    env.run(); 
  end

  final
    $display("******************* End of testcase ****************");
endprogram
