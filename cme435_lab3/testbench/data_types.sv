`define P1 0
`define P2 1
`define P3 2
`define P4 3

parameter NEWDATA_LEN_WIDTH  = 5;
typedef logic [NEWDATA_LEN_WIDTH-1:0]  new_len_t;

parameter OUTPUT_DATA_BUS_WIDTH = 8;
typedef logic [OUTPUT_DATA_BUS_WIDTH-1:0] out_data_t;

parameter INPUT_BUS_WIDTH = 8;
typedef logic [INPUT_BUS_WIDTH-1:0] in_data_t;