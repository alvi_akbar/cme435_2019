`ifndef DATA_TYPES
   `define DATA_TYPES
   `include "testbench/data_types.sv"
`endif


interface output_intf(input logic clk, input logic rst_b);
	out_data_t      data_out_1,
	                data_out_2,
	                data_out_3,
                        data_out_4;
        
	new_len_t       newdata_len_1,
	                newdata_len_2,
	                newdata_len_3,
                        newdata_len_4;
        
        logic           proceed_1,
                        proceed_2,
                        proceed_3,
                        proceed_4;
        
        clocking cb @ (posedge clk);
                input data_out_1;
                input data_out_2;
                input data_out_3;
                input data_out_4;
                input newdata_len_1;
                input newdata_len_2;
                input newdata_len_3;
                input newdata_len_4;
                output proceed_1;
                output proceed_2;
                output proceed_3;
                output proceed_4;
        endclocking

        /* UPSTREAM: dut -> monitor */
	modport OP(     
                input clk,
		input rst_b,
		clocking cb
                );
        
endinterface: output_intf

interface input_intf(input logic clk, input logic rst_b);
        in_data_t       data_in;
        
        logic           ack, 
                        bnd_plse;

        clocking cb @ (posedge clk);
                output data_in;
                input ack;
                output bnd_plse;
        endclocking

        /* DOWNSTREAM: driver -> dut */
	modport IP(
                input clk,
                input rst_b,
                clocking cb
                );
                
endinterface: input_intf

interface mon_intf(input logic clk, input logic rst_b);
        in_data_t       data_in;
        
        logic           ack,
                        bnd_plse;
        
        clocking cb @ (posedge clk);
        endclocking

        /* COMMON */
	modport MON(
                input clk,
                input rst_b
                );

endinterface: mon_intf