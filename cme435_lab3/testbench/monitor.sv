`ifndef DATA_TYPES
   `define DATA_TYPES
   `include "testbench/data_types.sv"
`endif

`ifndef TRANSACTION 
  `define TRANSACTION
  `include "testbench/transaction.sv"
`endif

class monitor;

  //creating virtual interface handle
   virtual output_intf.OP vif;
   transaction trans;
   
   //creating mailbox handle
   mailbox mon2scb;

  //constructor
  function new(virtual output_intf.OP vif, mailbox mon2scb);
    //getting the interface
    this.vif = vif;
    //getting the mailbox handles from environment
    this.mon2scb = mon2scb;

  endfunction

task main;
  forever begin
    trans = new();

    if (vif.cb.newdata_len_1)
      vif.cb.proceed_1 <= 1'b1;

    if (vif.cb.newdata_len_2)
      vif.cb.proceed_2 <= 1'b1;

    if (vif.cb.newdata_len_3)
      vif.cb.proceed_3 <= 1'b1;

    if (vif.cb.newdata_len_4)
      vif.cb.proceed_4 <= 1'b1;
    
    @(posedge vif.clk);
    vif.cb.proceed_1 <= 1'b0;
    vif.cb.proceed_2 <= 1'b0;
    vif.cb.proceed_3 <= 1'b0;
    vif.cb.proceed_4 <= 1'b0;

    trans.data_out_1 <= vif.cb.data_out_1;
    trans.data_out_2 <= vif.cb.data_out_2;
    trans.data_out_3 <= vif.cb.data_out_3;
    trans.data_out_4 <= vif.cb.data_out_4;

    trans.newdata_len_1 <= vif.cb.newdata_len_1;
    trans.newdata_len_2 <= vif.cb.newdata_len_2;
    trans.newdata_len_3 <= vif.cb.newdata_len_3;
    trans.newdata_len_4 <= vif.cb.newdata_len_4;
  

    @(posedge vif.clk);
    mon2scb.put(trans);
    trans.display("[ Monitor ]");
  end

endtask

endclass