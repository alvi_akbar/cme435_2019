`ifndef DATA_TYPES
   `define DATA_TYPES
   `include "testbench/data_types.sv"
`endif

class pkt_t;
  rand in_data_t dst_addr;
  rand in_data_t payload[$];
  rand int size;

  constraint dst_addr_c {dst_addr inside {0, 1, 2, 3}; }
  constraint payload_size_c { payload.size() inside {4, 8, 12, 16}; }
  constraint length_c { size == payload.size(); }

endclass

class transaction;
  rand pkt_t pkt;

  bit bnd_plse;
  bit proceed [0:3];
  
  in_data_t data_in;

  out_data_t data_out_1;
  out_data_t data_out_2;
  out_data_t data_out_3;
  out_data_t data_out_4;
  
  new_len_t newdata_len_1;
  new_len_t newdata_len_2;
  new_len_t newdata_len_3;
  new_len_t newdata_len_4;

  function new();
    pkt = new;
    proceed = '{default:0};
    proceed[pkt.dst_addr] = 1'b1;
  endfunction

  function void display(string name); 
    $display("-\n[%s]: ---------- PACKET ----------", name); 
    $display("pkt dst_addr: %0d", pkt.dst_addr);
    $display("payload data: ");
    
    foreach(pkt.payload[i])
        $write("%3d : %h \n", i+1, pkt.payload[i]); 

    // foreach(proceed[i])
    //   $display("- proceed_%0d = %h", i+1, proceed[i]);

    // $display("- new_data_len_1 = %h", newdata_len_1);
    // $display("- new_data_len_2 = %h", newdata_len_2);
    // $display("- new_data_len_3 = %h", newdata_len_3);
    // $display("- new_data_len_4 = %h", newdata_len_4);

    $display("- data_out_1 = %0d", data_out_1);
    $display("- data_out_2 = %0d", data_out_2);
    $display("- data_out_3 = %0d", data_out_3);
    $display("- data_out_4 = %0d", data_out_4);
  endfunction
  
endclass