`ifndef TRANSACTION
  `define TRANSACTION
  `include "testbench/transaction.sv"
`endif

class scoreboard;
    //creating mailbox handle
    mailbox mon2scb;

    //count the number of transactions
    int no_transactions; 
    transaction trans;


    //constructor
    function new(mailbox mon2scb);
    //getting the mailbox handles from environment
        this.mon2scb = mon2scb;
        this.no_transactions = 0;
    endfunction

    //Compares the actual result with the expected result
    task main();
        forever begin
            mon2scb.get(trans);
            if((trans.newdata_len_1) != 0)
                begin
                    if(trans.data_in == trans.data_out_1)
                        $error("Wrong Result.\n\t");
                end
            
            if((trans.newdata_len_2) != 0)
                begin
                    if(trans.data_in != trans.data_out_2)
                        $error("Wrong Result.\n\t");
                end
                
            if((trans.newdata_len_3) != 0)
                begin
                    if(trans.data_in != trans.data_out_3)
                        $error("Wrong Result.\n\t");
                end
            
            if((trans.newdata_len_4) != 0)
                begin
                    if(trans.data_in != trans.data_out_4)
                        $error("Wrong Result.\n\t");
                end


                no_transactions++;
                trans.display("[ Scoreboard ]");

        end
    endtask
endclass
