`ifndef INTERFACE
  `define INTERFACE 
  `include "testbench/interface.sv"
`endif

`ifndef TRANSACTION
  `define TRANSACTION
  `include "testbench/transaction.sv"
`endif

`ifndef GENERATOR
  `define GENERATOR
  `include "testbench/generator.sv"
`endif

`ifndef DRIVER
  `define DRIVER
  `include "testbench/driver.sv"
`endif

`ifndef MONITOR
  `define MONITOR
  `include "testbench/monitor.sv"
`endif

`ifndef SCOREBOARD
  `define SCOREBOARD
  `include "testbench/scoreboard.sv"
`endif

class environment;
 
  // generator instance
  generator gen;
  driver driv;
  monitor mon;
  scoreboard scb;

  //mailbox handle's
  mailbox gen2driv;
  mailbox mon2scb;

  //virtual interface
  virtual mon_intf.MON i_mon_intf;
  virtual input_intf.IP i_in_intf;
  virtual output_intf.OP i_out_intf;

  //constructor
  function new(virtual mon_intf.MON i_mon_intf,
               virtual input_intf.IP i_in_intf,
               virtual output_intf.OP i_out_intf);
    
    //get the interface from test
    this.i_mon_intf = i_mon_intf; 
    this.i_in_intf = i_in_intf; 
    this.i_out_intf = i_out_intf;

    $display("%0d : Environment : created env object", $time);
    
    //creating the mailbox (Same handle will be shared across generator and driver)
    gen2driv = new();
    mon2scb = new();

    // creating generator
    gen = new(i_in_intf, gen2driv);
    driv = new(i_in_intf, gen2driv);

    mon = new(i_out_intf, mon2scb);
    scb = new(mon2scb);

  endfunction

  task pre_test();    
    $display("%0d : Environment : start of pre_test()", $time);
    reset();
    $display("%0d : Environment : end of pre_test()", $time);
  endtask

  task test();
    $display("%0d : Environment : start of test()", $time);

    fork
      gen.main();
      driv.main();
      mon.main();
      scb.main();
    join_any
    
    wait(gen.ended.triggered); // optional
    // wait(gen.repeat_count == driv.no_transactions);
    // wait(gen.repeat_count == scb.no_transactions);

    $display("%0d : Environment : end of test()", $time);
  endtask

  task post_test();
    $display("%0d : Environment : start of post_test()", $time);
    
    if($root.tbench_top.error_count != 0)
      $display("############# TEST FAILED #############");
    else
      $display("############# TEST PASSED #############");

    $display("%0d : Environment : end of post_test()", $time);

  endtask

  task reset();
    wait(!i_in_intf.rst_b);
    $display("[ ENVIRONMENT ] ----- Reset Started -----");
    
    $root.tbench_top.error_count <= 0;

    i_out_intf.cb.proceed_1 <= 1'b0;
    i_out_intf.cb.proceed_2 <= 1'b0;
    i_out_intf.cb.proceed_3 <= 1'b0;
    i_out_intf.cb.proceed_4 <= 1'b0;

    i_in_intf.cb.bnd_plse <= 1'b0;
    i_in_intf.cb.data_in <= 8'h00;

    wait(i_in_intf.rst_b);

    $display("[ ENVIRONMENT ] ----- Reset Ended -------");
    endtask

  //run task
  task run();
    $display("%0d : Environment : start of run()", $time);

    pre_test();
    test();
    post_test();

    $display("%0d : Environment : end of run()", $time);
  endtask


endclass
