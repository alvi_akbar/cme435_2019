quit -sim
onbreak {resume}
# create library
if [file exists work] {
 vdel -all
}

vlib work
vmap work work

# compiling all dut source files
vlog dut/pdm_core.svp
vlog dut/dut_top.sv

# compiling all testbench source files
vlog testbench/*.sv 

# optimize design
vopt +acc tbench_top -o tbench_top_opt
vsim tbench_top_opt

# wave signals
add wave /tbench_top/*
do wave.do

add log -r /*

# run simulation
# run –all

run 2000

# quit –f