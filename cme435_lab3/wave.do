onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tbench_top/clk
add wave -noupdate /tbench_top/rst_b
add wave -noupdate /tbench_top/error
add wave -noupdate -radix decimal /tbench_top/error_count
add wave -noupdate -divider {Input Interface}
add wave -noupdate /tbench_top/i_in_intf/bnd_plse
add wave -noupdate /tbench_top/i_in_intf/ack
add wave -noupdate /tbench_top/i_in_intf/data_in
add wave -noupdate -divider {Output Port 1}
add wave -noupdate /tbench_top/i_out_intf/newdata_len_1
add wave -noupdate /tbench_top/i_out_intf/proceed_1
add wave -noupdate /tbench_top/i_out_intf/data_out_1
add wave -noupdate -divider {Output Port 2}
add wave -noupdate /tbench_top/i_out_intf/newdata_len_2
add wave -noupdate /tbench_top/i_out_intf/proceed_2
add wave -noupdate /tbench_top/i_out_intf/data_out_2
add wave -noupdate -divider {Output Port 3}
add wave -noupdate /tbench_top/i_out_intf/newdata_len_3
add wave -noupdate /tbench_top/i_out_intf/proceed_3
add wave -noupdate /tbench_top/i_out_intf/data_out_3
add wave -noupdate -divider {Output Port 4}
add wave -noupdate /tbench_top/i_out_intf/newdata_len_4
add wave -noupdate /tbench_top/i_out_intf/proceed_4
add wave -noupdate /tbench_top/i_out_intf/data_out_4
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 4} {14 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 138
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {65 ns}
