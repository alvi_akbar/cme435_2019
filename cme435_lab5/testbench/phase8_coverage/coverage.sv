`include "testbench/phase3_base/transaction.sv"

`ifndef COVERAGE_SV
`define COVERAGE_SV

class coverage;
	transaction trans;

	covergroup cov_drv;
		address : coverpoint trans.pkt.dst_addr {
			bins port_1 = {1};
			bins port_2 = {2};
			bins port_3 = {3};
			bins port_4 = {4};
		}

		size : coverpoint trans.pkt.payload.size {
			bins four = {4};
			bins eight = {8};
			bins twelve = {12};
			bins sixteen = {16};
		}
		
		cross address, size;

	endgroup

	covergroup cov_data with function sample(logic payload);
		data : coverpoint payload {
			bins low = {[0:20]};
			bins mid = {[21:150]};
			bins high = {[151:255]};
		}
	endgroup

	function new();
		cov_drv = new();
		cov_data = new();
	endfunction : new

	task sample( transaction trans );
		this.trans = trans;
		cov_drv.sample();
	endtask : sample

endclass

`endif