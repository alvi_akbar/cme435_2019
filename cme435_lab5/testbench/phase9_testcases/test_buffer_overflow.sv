`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"
`include "testbench/phase3_base/transaction.sv"

program testbench(
    intf i_intf
);

    /* declaring environment instance */
    environment env;
    
    initial begin

        /* creating environment */
        env = new (i_intf);

        env.gen.repeat_count = 100;
        i_intf.error_override = 1;

        $display("******************* Start of TESTCASE_BUFFER_OVERFLOW ****************");
        
        /* calling run of env, it in turns calls other main tasks */
        env.run();
    end

    final
        $display("******************* End of testcase_buffer_overflow ****************");
            
endprogram : testbench