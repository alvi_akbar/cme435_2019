`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"
`include "testbench/phase3_base/transaction.sv"

program testbench(intf i_intf);

    class trans_max extends transaction;
        
        constraint payload_size_c{
            pkt.payload.size == 17;
        }

    endclass : trans_max

    /* declaring environment instance */
    environment #(trans_max) env;

    initial begin
        /* creating environment */
        env = new (i_intf);

        /* setting the repeat count of generator such as 5, means to generate 5 packets */
        env.gen.repeat_count = 10;

        $display("******************* Start of testbench ****************");
        /* calling run of env, it in turns calls other main tasks */
        env.run();
    end

    final
        $display("******************* End of testbench ****************");
             
endprogram : testbench
