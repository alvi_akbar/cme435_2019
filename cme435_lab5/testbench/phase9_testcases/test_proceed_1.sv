`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"
`include "testbench/phase3_base/transaction.sv"

program testbench(intf i_intf);

    class trans_max extends transaction;
        
        constraint dst_addr_c{
            pkt.dst_addr == 1;
        }

    endclass : trans_max

    /* declaring environment instance */
    environment #(trans_max) env;

    task test_proceed_1(input int pkt_count);
        repeat(pkt_count) begin
            wait(i_intf.ack);
            @(posedge i_intf.clk);
            proceed_1_asserted_in_next_clock_cycle: assert (i_intf.proceed_1 == 1)
                $display("%t : test_proceed_1: Assertion Successful!", $time);
                else begin
                    ->i_intf.error;
                    $error("%t test_proceed_1:  Wrong Result!", $time);
                end

        end
    endtask : test_proceed_1

    initial begin
        /* creating environment */
        env = new (i_intf);

        /* setting the repeat count of generator such as 5, means to generate 5 packets */
        env.gen.repeat_count = 10;
        i_intf.error_override = 1;

        $display("******************* Start of testbench ****************");
        /* calling run of env, it in turns calls other main tasks */
       fork 
        env.run();
        test_proceed_1(9);
       join
    end

    final
        $display("******************* End of testbench ****************");
             
endprogram : testbench
