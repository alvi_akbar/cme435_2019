`timescale 1ns/1ns

`include "testbench/utils/data_types.sv"
`include "testbench/phase1_top/interface.sv"
`include "testbench/phase2_environment/environment.sv"

program testbench(intf i_intf);

    /* declaring environment instance */
    environment env;

    task test_bnd_asserted_one_clock_cycle(input int pkt_count);
        repeat (pkt_count) begin
            @(posedge i_intf.clk);
            wait(i_intf.bnd_plse == 1);

            $display("------------------------------------------------------------------------------------");
            $display("%t : BND TEST 1 : Test BND Asserted for one Clock Cycle", $time);
            $display("------------------------------------------------------------------------------------");
            if(i_intf.data_in != 0) 
                continue;

            bnd_high_1_cycle: assert (i_intf.bnd_plse == 1)
                $display("%t : test_bnd_asserted_one_clock_cycle: Assertion Successful!", $time);
                else begin
                    ->i_intf.error;
                    $error("%t : test_bnd_asserted_one_clock_cycle : Wrong Result!", $time);
                end
        end
    endtask

    task test_correct_dst_addr_when_bnd_high(input int pkt_count);
        repeat (pkt_count) begin
            wait(i_intf.bnd_plse == 1);
            @(posedge i_intf.clk);

            $display("------------------------------------------------------------------------------------");
            $display("%t : BND TEST 2 : Test Correct Data Address when BND HIGH", $time);
            $display("------------------------------------------------------------------------------------");
            if(i_intf.data_in != 0) 
                continue;

            @i_intf.bnd_plse;

            correct_dst_addr_on_bnd: assert (i_intf.data_in == env.gen.trans.pkt.payload[0])
                $display("%t : test_correct_dst_addr_when_bnd_high: Assertion Successful!", $time);
                else begin
                    ->i_intf.error;
                    $error("%t : test_correct_dst_addr_when_bnd_high : \n\t Actual:  %0d  Expected:  %0d", $time,
                    i_intf.data_in, env.gen.trans.pkt.payload[0]);
                end
        end
    endtask

    task test_bnd_consides_with_last_payload_byte(input int pkt_count);
        repeat (pkt_count) begin
            @(posedge i_intf.clk);

            wait(i_intf.data_in == 0);
            wait(i_intf.bnd_plse == 1);

            @(posedge i_intf.clk);
            wait(i_intf.data_in == 0);

            @(posedge i_intf.clk);
            bnd_consides_with_last_payload_byte: assert (i_intf.data_in == 0)
                $display("%t : test_bnd_high_1_cycle: Assertion Successful!", $time);
                else begin
                    ->i_intf.error;
                    $error("%t : test_bnd_high_1_cycle : Wrong Result!\n\t Data in value is %0d", $time, i_intf.data_in);
                end
        end
    endtask

    initial begin
        /* creating environment */
        env = new (i_intf);

        /* setting the repeat count of generator such as 5, means to generate 5 packets */
        env.gen.repeat_count = 10;
        i_intf.error_override = 1;
        
        $display("******************* Start of TESTCASE_BND_PLSE ****************");
        fork
            test_bnd_asserted_one_clock_cycle(env.gen.repeat_count);
            test_correct_dst_addr_when_bnd_high(env.gen.repeat_count);
            // test_bnd_consides_with_last_payload_byte(env.gen.repeat_count);
            env.run();
        join
    end

    final
        $display("******************* End of TESTCASE_BND_PLSE ****************");

endprogram : testbench