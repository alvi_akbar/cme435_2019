`include "testbench/utils/data_types.sv"

`ifndef TRANSACTION_SV
`define TRANSACTION_SV

class pkt_t;
    
    rand in_data_t dst_addr;
    rand in_data_t payload [$];

    rand int size;

    constraint dst_addr_c{
        dst_addr inside {1, 2, 3, 4};
    }

    constraint payload_dst_byte_c{
        payload[0] == dst_addr;
    }

    constraint payload_size_c{
        payload.size() inside {5, 9, 13, 17};
    }

    constraint length_c{
        size == payload.size();
    }

    constraint order_c{
        solve dst_addr before payload;
        solve payload before size;
    }
endclass : pkt_t


/* Base class */
virtual class base_transaction;

    static int count; /* Number of instance created */
    int id; /* Transction id to keep track of transactions */

    function void post_randomize();
        id = count++;
    endfunction : post_randomize

    pure virtual function void display(string name="");

endclass : base_transaction


/* Original Transaction Class */
class transaction extends base_transaction;
    rand pkt_t pkt;

    bit bnd_plse;
    bit proceed [0:3];


    new_len_t newdata_len[0:3];

    out_data_t data_out;

    function new ();
        super.new();
        id = count;
        pkt = new ();
        
        data_out = 0;

        foreach ( proceed[i] ) proceed[i] = 0;

        foreach ( newdata_len[i] ) newdata_len[i] = 0;

    endfunction : new

    function void post_randomize();
        super.post_randomize();
        proceed[pkt.dst_addr-1] = 1'b1;
    endfunction : post_randomize

    function void display(string name);
        $display("\n[%s]:@%0t >>> TRANSACTION ID: %0d >>>",
            name, $time, id);

        $display("@%0d: pkt dst_addr: %0d", $time, pkt.dst_addr);

        $displayh("@%0d: payload: %p", $time, pkt.payload);

        $display("@%0d: len1 = %h \t len2 = %h \t len3 = %h \t len4 = %h",
        $time, newdata_len[0], newdata_len[1], newdata_len[2], newdata_len[3]);

        $display("@%0d: proceed_1 = %h \t proceed_2 = %h \t proceed_3 = %h \t proceed_4 = %h",
            $time, proceed[0], proceed[1], proceed[2], proceed[3]);

        $displayh("@%0d: data_out = %h", $time, data_out);

        $display("\n[%s]:@%0t <<< END DISPLAY TRANSACTION <<<", name, $time);
    endfunction : display

endclass : transaction

`endif
