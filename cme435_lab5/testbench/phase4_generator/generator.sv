`include "testbench/utils/data_types.sv"
`include "testbench/phase3_base/transaction.sv"

`ifndef GENERATOR_SV
`define GENERATOR_SV

class generator#(type T = transaction);
    rand T trans;

    virtual intf vif;

    mailbox gen2driv;

    /* repeat count, to specify number of items to generate */
    int repeat_count;

    /* event, to indicate the end of transaction generation */
    event ended;

    function new (virtual intf vif, mailbox gen2driv);
        this.vif = vif;
        this.gen2driv = gen2driv;
    endfunction

    task main();
        repeat (repeat_count) begin
            trans = new ();

            @vif.cbDriv;
            assert (trans.randomize)
                else $error("Gen:: trans randomization failed");

            gen2driv.put(trans);

            trans.display("[ GENERATOR ]");

            wait (vif.cbDriv.ack);

            repeat (4+4+1+trans.pkt.size) @vif.cbDriv;

        end
        -> ended; /* trigger end of generation */
    endtask : main

endclass : generator

`endif