# Contributing

No Contribution allowed at this point. Only the author has complete right over this material
* [Types of Contributions](#Types-of-Contributions)
* [Contributor Setup](#Setting-Up-the-Code-for-Local-Development)
* [Contributor Guidelines](#Contributor-Guidelines)
* [Contributor Testing](#Running-Coverage-with-csh)
* [Core Committer Guide](#Core-Committer-Guide)