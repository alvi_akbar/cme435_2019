# Credits

## Development Leads

* Alvi Akbar ([@alvi_akbar](https://bitbucket.org/alvi_akbar))

## Core Committers

* Alvi Akbar ([@alvi_akbar](https://bitbucket.org/alvi_akbar))

## Contributors

* Alvi Akbar ([@alvi_akbar](https://bitbucket.org/alvi_akbar))