quit -sim
.main clear

onbreak {resume}

# create library
if [file exists work] {
 vdel -all
}

vlib work
vmap work work

# compiling all DUT source files
vlog dut/*.svp
vlog dut/*.sv

# compiling all TESTBENCH source files
vlog testbench/utils/*.sv
vlog testbench/phase1_top/*.sv
vlog testbench/phase2_environment/*.sv
vlog testbench/phase3_base/*.sv
vlog testbench/phase4_generator/*.sv
vlog testbench/phase5_driver/*.sv
vlog testbench/phase6_monitor/*.sv
vlog testbench/phase7_scoreboard/*.sv
vlog testbench/phase9_testcases/test_max_payload_endpoint.sv


# optimize design
vopt +acc tbench_top -o tbench_top_opt
# Simulating with seed for randomization
vsim -sv_seed 100 tbench_top_opt

# wave signals
add wave /tbench_top/*

if [file exists wave.do] { do wave.do }
add log -r /*

# Debugging Logic
do scripts/breakpoints.do

# run -continue
# examine x to examine current state of something
# Debugging Ends here

# run simulation
run -all

# quit –f