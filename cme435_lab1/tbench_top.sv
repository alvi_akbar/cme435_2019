`timescale 1 ns / 1 ns

`define    INPUT    2'b00
`define    UP       2'b01
`define    DOWN     2'b10
`define    HOLD     2'b11

module tbench_top;

reg [7:0] data_in;
reg [1:0] s_in;

wire [7:0] data_out;

wire clk, reset;

clock_generator clk1(.clk(clk));

reset reset1(.rst(reset));

up_down_counter dut(
    .data_out(data_out),
    .data_in(data_in),
    .s_in(s_in),
    .clk_in(clk),
    .reset_in(reset)
);

testbench tb(
  .count(data_out),
  .data_in(data_in),
  .s_in(s_in),
  .reset(reset),
  .clk(clk)
);


initial
    $monitor($stime,, reset,, clk,,, data_out); 
endmodule