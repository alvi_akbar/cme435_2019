# Documentation

**LAB 01**  
**Name:** Alvi Akbar  
**NSID:** ala273  
**Student No:** 11118887

## Part 1

Default Simulation Run Length: `100 ns`

## Part 2

***Questa SIM Commands***

| Command      | Description                                                                                                                                                                                                                                      |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `vlib`       | This command creates a design library. You must use vlib rather than operating system commands to create a library directory or index file.                                                                                                      |
| `vmap`       | The vmap command defines a mapping between a logical library name and a directory by modifying the modelsim.ini file.                                                                                                                            |
| `vlog`       | The vlog command compiles Verilog source code and SystemVerilog extensions into a specified working library.                                                                                                                                     |
| `vcom`       | The vcom command compiles VHDL source code into a specified working library.                                                                                                                                                                     |
| `vopt`       | The vopt command performs global optimizations on designs after they have been compiled with vcom or vlog.                                                                                                                                       |
| `vsim`       | The vsim command invokes the VSIM simulator, which can be used to view the results of a previous simulation run.                                                                                                                                 |
| `run`        | This command advances the simulation by the specified number of timesteps.                                                                                                                                                                       |
| `step`       | The step command is an alias for the run command with the -step switch.The step command is an alias for the run command with the -step switch.                                                                                                   |
| `bp`         | This command sets either a file-line breakpoint or returns a list of currently set breakpoints. It allows enum names, as well as literal values, to be used in condition expressions.                                                            |
| `log`        | This command creates a wave log format (WLF) file containing simulation data for all objects whose names match the provided specifications.                                                                                                      |
| `show`       | This command lists objects and subregions visible from the current environment.                                                                                                                                                                  |
| `do`         | This command executes the commands contained in a DO file.                                                                                                                                                                                       |
| `restart -f` | This command reloads the design elements and resets the simulation time to zero. Only design elements that have changed are reloaded. `-force` Specifies that the simulation will be restarted without requiring confirmation in a popup window. |
| `view`       | This command opens the specified window. If you specify this command without arguments it returns a list of all open windows in the current layout.                                                                                              |
All the above definations are taken from [ReferenceQuesta SIM Command Reference Manual](https://www.engr.usask.ca/classes/CME/435/mentor/htmldocs/mgchelp.htm#context=questa_sim_ref&id=37&tag=4478873).

## Part 3
*** Test Logs***
```
# TEST 1: Test if data_in assignment success.
# 
# Expected value: data_in = 8'h3
# 
# Successful: data_in equals 03.
# 
# 
# TEST 2: Test if s_in assignment success.
# 
# Expected result: s_in equals 00.
# 
#          0 0 0    x
#          0 0 0    x
#         10 0 1    x
# Successful: s_in equals 0.
# 
# 
#         20 0 0    x
#         20 0 0    x
#         30 0 1    3
# TEST 3: Test INPUT Operation Mode success.
# 
# Expected: data_in passed to data_out on posedge clock.
# 
# Successful: Hold Operation Mode successful.
# 
# 
#         40 0 0    3
# TEST 4: Test UP Operation Mode success.
# 
# Expected: data_out increments by 1.
# 
#         40 0 0    4
#         50 0 1    5
# Successful: UP Operation Mode successful.
# 
# 
#         60 0 0    5
#         70 0 1    6
#         80 0 0    6
# TEST 5: Test DOWN Operation Mode success.
# 
#         80 0 0    6
#         90 0 1    5
# Successful: DOWN Operation Mode successful.
# 
# 
# TEST 6: Test Rollback Operation Mode success.
# 
#        100 0 0    5
#        110 0 1    4
#        120 0 0    4
#        120 0 0    4
#        130 0 1    0
# Successful: Rollback Operation Mode successful.
# 
# 
#        140 0 0    1
#        150 0 1    2
#        160 0 0    2
#        170 0 1    3
#        180 0 0    3
#        190 0 1    4
#        200 0 0    4
#        210 0 1    5
#        220 0 0    5
#        230 0 1    6
#        240 0 0    6
#        250 0 1    7
#        260 0 0    7
#        270 0 1    8
#        280 0 0    8
#        290 0 1    9
#        300 0 0    9
#        310 0 1   10
#        320 0 0   10
#        330 0 1   11
#        340 0 0   11
#        350 0 1   12
#        360 0 0   12
#        370 0 1   13
#        380 0 0   13
#        390 0 1   14
#        400 0 0   14
#        410 0 1   15
#        420 0 0   15
#        430 0 1   16
#        440 0 0   16
#        450 0 1   17
#        460 0 0   17
#        470 0 1   18
#        480 0 0   18
#        490 0 1   19
```