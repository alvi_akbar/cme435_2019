`timescale 1 ns / 1 ns

module clock_generator(clk);
output reg clk;

initial // Clock generator
  begin
    clk = 0;
    forever #10 clk = !clk;
  end
endmodule