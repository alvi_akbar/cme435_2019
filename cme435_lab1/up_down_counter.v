`timescale 1 ns / 1 ns

`define    INPUT    2'b00
`define    UP       2'b01
`define    DOWN     2'b10
`define    HOLD     2'b11

module up_down_counter(
    data_out,   // Data output
    data_in,    // Present data input
    s_in,       // Mode control  00: Present parallel data input.
                //               01: Increment
                //               10: Decrement
                //               11: Hold
    clk_in,     // clock input
    reset_in    // reset input
);

output reg [7:0] data_out;

input [7:0] data_in;
input [1:0] s_in;
input clk_in, reset_in;

always @ (posedge clk_in or posedge s_in)
    case (s_in)
        `INPUT   : data_out = data_in;
        `UP      : data_out <= data_out + 8'h01;
        `DOWN    : data_out <= data_out - 8'h01;
        `HOLD    : data_out <= data_out;
        default : data_out <= data_out;
    endcase

always @ (posedge clk_in)
  if (reset_in || data_out > 8'h0F)
    data_out = 8'h00;
  else
    data_out = data_out;
    
endmodule
