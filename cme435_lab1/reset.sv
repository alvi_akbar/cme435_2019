`timescale 1 ns / 1 ns

module reset(rst);
output reg rst;

initial
begin
    rst = 0;
    // #5 rst = 1;
    // #10 rst = 0;
    // #5 rst = 1;
    // #10 rst = 0;
end

endmodule
