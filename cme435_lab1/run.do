onbreak {resume}
# create library
if [file exists work] {
 vdel -all
}

vlib work
vmap work work

# compiling all source files
vlog counter.v test_counter.v

# optimize design
vopt +acc test_counter -o test_counter_opt
vsim test_counter_opt

# wave signals
add wave /top/p/* 

add log -r /*

# run simulation
run –all  

quit –f