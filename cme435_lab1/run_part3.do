onbreak {resume}
# create library
if [file exists work] {
 vdel -all
}

vlib work
vmap work work

# compiling all source files
vlog up_down_counter.v tbench_top.sv clock_generator.sv reset.sv testbench.sv

# optimize design
vopt +acc tbench_top -o tbench_top_opt
vsim tbench_top_opt

# wave signals
add wave /tbench_top/* 

add log -r /*

run simulation
# run –all

run 500

# quit –f