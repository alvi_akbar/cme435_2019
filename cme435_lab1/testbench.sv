`timescale 1 ns / 1 ns

`define    INPUT    2'b00
`define    UP       2'b01
`define    DOWN     2'b10
`define    HOLD     2'b11

module testbench(
    count,
    data_in,
    s_in,
    reset,
    clk);

output reg [7:0] data_in;
output reg [1:0] s_in;

input [7:0] count;
input clk, reset;


initial
begin
    
    /**
     * TEST 1
     */

    $display("TEST 1: Test if data_in assignment success.\n");
    data_in = 8'h03;

    $display("Expected value: data_in = 8'h3\n");
    assert (data_in == 8'h03) $display("Successful: data_in equals %h.\n\n", 8'h03);
    else $error("Fail: Wrong data_in value: %h\n\n", data_in);

    @(negedge clk);
   /**
     * TEST 2
     */ 

    $display("TEST 2: Test if s_in assignment success.\n");
    $display("Expected result: s_in equals %h.\n", 8'h0);

    $display($stime,, reset,, clk,,, count);     

    @(negedge clk);
    s_in = `INPUT;

    assert(s_in == `INPUT) $display("Successful: s_in equals %h.\n\n", 2'h0);
    else $error("Fail: Wrong s_in value: %h.\n\n", s_in);

    $display($stime,, reset,, clk,,, count);     

    @(negedge clk);
    /**
     * TEST 3
     */

    $display("TEST 3: Test INPUT Operation Mode success.\n");
    $display("Expected: data_in passed to data_out on posedge clock.\n");
    
    assert(count == 8'h03) $display("Successful: Hold Operation Mode successful.\n\n");
    else $error("Fail: data_in not passed to data_out next posedge clock.\n\n");;
    $display($stime,, reset,, clk,,, count);     
    
    /**
     * TEST 4
     */

    s_in = `UP;

    $display("TEST 4: Test UP Operation Mode success.\n");
    $display("Expected: data_out increments by 1.\n");

    @(negedge clk);

    assert(count == 8'h05) $display("Successful: UP Operation Mode successful.\n\n");
    else $error("Fail: count not incremented by 1.\n\n");


    @(negedge clk);
    s_in = `DOWN;

    $display($stime,, reset,, clk,,, count);  

    $display("TEST 5: Test DOWN Operation Mode success.\n");
    @(negedge clk);
    assert(count == 8'h05) $display("Successful: DOWN Operation Mode successful.\n\n");
    else $error("Fail: count not decremented by 1.\n\n");


    $display("TEST 6: Test Rollback Operation Mode success.\n");

    data_in = 8'hFF;

    @(negedge clk);
    s_in = `INPUT;

    $display($stime,, reset,, clk,,, count);  

    @(negedge clk);

    s_in = `UP;

    assert(count == 8'h00) $display("Successful: Rollback Operation Mode successful.\n\n");
    else $error("Fail: Rollback unsuccessful.\n\n");

end

endmodule